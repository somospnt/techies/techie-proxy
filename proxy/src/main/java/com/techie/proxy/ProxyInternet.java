package com.techie.proxy;

import com.techie.proxy.domain.Internet;
import com.techie.proxy.domain.impl.InternetReal;
import java.util.ArrayList;
import java.util.List;

public class ProxyInternet implements Internet {

    private Internet internet;
    private static List<String> sitiosProhibidos;

    static {
        sitiosProhibidos = new ArrayList<String>();
        sitiosProhibidos.add("youtube.com");
        sitiosProhibidos.add("facebook.com");
        sitiosProhibidos.add("twitter.com");
    }

    public ProxyInternet() {
        this.internet = new InternetReal();
    }

    @Override
    public void conectarseA(String url) {
        System.out.println("///////////////////////////");
        System.out.println("Conectandose a: " + url);
        System.out.println("///////////////////////////");
        System.out.println("///////////////////////////");
        System.out.println("///////////////////////////");
        if (sitiosProhibidos.contains(url)) {
            throw new IllegalArgumentException("No se puede acceder a este sitio: " + url);
        }
        internet.conectarseA(url);
    }

}
