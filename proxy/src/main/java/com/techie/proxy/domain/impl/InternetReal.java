package com.techie.proxy.domain.impl;

import com.techie.proxy.domain.Internet;

public class InternetReal implements Internet {

    @Override
    public void conectarseA(String url) {
        System.out.println("///////////////////////////");
        System.out.println("Te conectaste a: " + url);
    }

}
