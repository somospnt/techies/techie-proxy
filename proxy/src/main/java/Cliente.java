
import com.techie.proxy.ProxyInternet;
import com.techie.proxy.domain.Internet;

public class Cliente {

    public static void main(String[] args) {

        Internet internet = new ProxyInternet();
        try {
            internet.conectarseA("facebook.com");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        } finally {
            internet.conectarseA("somospnt.com");        
        }
    }
}
